#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct address{
	int pid; //to id ths diergasias pou tha paei na psaksei
	char page_number[6]; //p#, page number
	char offset[4]; //ta 3 teleutaia psifia offset
	char status[2]; //an einai R h W
};


struct inverted_page_table{
	int pid;
	char page_number[6];
};


struct memory{
	char status[2];
};

int main(int argc,char* argv[]){

int k;
int max_frames; //plithos twn plaisiwn
int q;
int max_traces; //plithos dieythynsewn apo kathe arxeio

if(argc!=5){
	printf("Error,not enough arguments\n");
}else{
	k = atoi(argv[1]);
	max_frames = atoi(argv[2]);
	q = atoi(argv[3]);
	max_traces = atoi(argv[4]);
}



FILE *fp1; //gia ta 2 arxeia
FILE *fp2;

fp1 = fopen("bzip.trace","r");
fp2 = fopen("gcc.trace","r");


struct memory main_memory[max_frames]; //prosomoiwsh ths kyrias mnhmhs pou apothikeyei an einai R h W h selida
struct inverted_page_table ipt[max_frames]; //desmeysh tou pinaka
int ipt_index=0; //deixnei mexri pou exei desmeytei o ipt
struct address addr; //oi dieythinseis pou pairnoume apo to arxeio
char buff[11]; //pinakas pou tha exei mesa mia grammh apo to arxeio se kathe epanalipsi
int i; //o index tou buff
char c; //tha bohthisei gia ton buff
int overall_page_faults = 0;
int write_back_counter=0; //oses selides einai dirty kai kanoun write back ston disko
int read_counter = 0; //oses selides apla kanoun read



int allagi=q; //boithitikh gia na kseroume pote tha pame sto allo arxeio


typedef enum {false,true} bool; //dyadikh metablhth pou tha mas bohthisei sthn ylopoihsh
bool flag;
flag = true;


int pf1=0; //page fault gia diergasia 1
int pf2=0; // <<		<<	    2



int j;
/***********************************/
//edo ksekinane oi epanalipseis
for(j=0;j<2*max_traces;j++){ //2*max_traces giati tha paroume kai ta 2 arxeia
	i=0; //arxikopoiw gia to buff
	if(flag == true){ //pairnw kataxwrhseis apo to prwto arxeio, tha ginei false otan kanw q epanalipseis
		while((c=fgetc(fp1))!= '\n'){
			buff[i] = c;
			i++;
		}
		addr.pid = 1; // gia na kserw apo poio arxeio to exw parei
	}else if(flag == false){
		while((c=fgetc(fp2))!= '\n'){
			buff[i] = c;
			i++;
		}
		addr.pid = 2;
	}
	allagi = allagi -1; //oi q epanalipseis meiwnontai


	if(allagi == 0){ //an ginoun oi q epanalipseis gia ena arxeio
		flag = !flag; //ara apo dw kai pera gia q epanalipseis pernoume apo to allo arxeio
		allagi = q; //opws arxika
	}

	//otan teleiwsei, o buff tha exei mesa olh thn grammh enos arxeioy
	//pame na thn perasoume sto struct
	for(i=0;i<5;i++){
		addr.page_number[i] = buff[i];
	}
	addr.page_number[5] = '\0'; //prokeimenou na ektypwsei swsta to apotelesma

	for(i=5;i<8;i++){ //ta 3 teleutaia 16dika bits
		addr.offset[i-5] = buff[i];
	}
	addr.offset[3] = '\0';

	addr.status[0] = buff[9];//to R h to W
	addr.status[1] = '\0';



/***************************/
//edw arxizoume kai bazoume ta stoixeia ston pinaka
	int counter=0; //synthiki ean pid h p# einai to idio

	if(ipt_index == 0){ //gia thn prwth epanalipsi
		ipt[ipt_index].pid = addr.pid;
		strcpy(ipt[ipt_index].page_number,addr.page_number);
		
		if(flag == true){ //gia ta page faults
			pf1++; //auksanontai ta pf gia thn 1h diergasia
			overall_page_faults++;
		}else if(flag == false){
			pf2++;
			overall_page_faults++;
		}

		strcpy(main_memory[ipt_index].status,addr.status); //bazw an  einai R h W


		if(main_memory[ipt_index].status[0] == 'R'){
			read_counter++;
		}


		ipt_index++; //H thesi ipt_index den exei "desmeytei" apla einai etoimh gia desmeysh
	}else{ // apo th deyterh epanalipsi kai meta
		int thesi =0; //mas boithaei sto skanarisma tou ipt pinaka

		//skanarw ton pinaka mexri na brw an id kai p# yparxoyn
		while((thesi<ipt_index) && (counter<2)){
			if(addr.pid == ipt[thesi].pid){
				counter++;
				if(strcmp(addr.page_number,ipt[thesi].page_number) == 0){
					counter++;
				}else{	//prepei na isxyoyn kai oi dyo periptwseis gia na exoume match alliws o counter mhdenizetai
					counter = 0;
				}
			}else{
				counter = 0;
			}
			thesi++;
		}
		//an o counter einai 2 dld exoume match!
		if(counter == 2){ //yparxei sth mnhmh
			printf("page %s is already in memory!\n",addr.page_number);
		}else{//edw tha baloume th selida sti mnhmh kai an xreiastei kanoume fwf
			printf("page %s is not in memory!\n",addr.page_number);
			int synthiki_pf=0; //me auth tha doume an h diergasia 1 h h 2 exei k+1 pf


			if(flag == true){
				pf1++;
				overall_page_faults++;
				if(pf1 == (k+1)){

					synthiki_pf = 1; //kseroume oti h diergasia 1 tha kanei fwf
					pf1=0;//ksekiname pali apo tin arxh
				}
			}else if(flag == false){
				pf2++;
				overall_page_faults++;
				if(pf2 == (k+1)){
					synthiki_pf = 2;
					pf2=0; 
				}
			}

			//************ FWF ************//
			if(synthiki_pf != 0){
				int u;
				printf("FWF for process:%d is in progress\n",addr.pid);
				for(u=0;u<ipt_index;u++){
					if(ipt[u].pid == synthiki_pf){
						//dld an broume mia selida ths diergasias
						if(main_memory[u].status[0] == 'W'){ //katharizoume th mnhmh apo thn selida
							write_back_counter++;
						}
						main_memory[u].status[0] = '\0';
						ipt[u].pid = -1; //leme kai kala oti h thesi ayth einai eleytheri
					}
				}
				printf("FWF is done\n");
			}

			//efoson exoume kanei (an exoume kanei) FWF pame na baloume to stoixeio ston pinaka efoson den yparxei ekei hdh
			int u=0;
			int save = 0; //tha ginei 1 an h selida apothikeytei
			while((u<ipt_index) && (save ==0)){
				if(ipt[u].pid == -1){
					ipt[u].pid = addr.pid;
					strcpy(ipt[u].page_number,addr.page_number);
					main_memory[u].status[0] = addr.status[0];
					if(main_memory[u].status[0] == 'R'){
						read_counter++;
					}
					save=1;
				}
				u++;
			}//telos ths while

			//AN H WHILE EINAI ANEPITYXHS PREPEI NA DESMEYSOUME MIA NEA THESI STOYS DYO PINAKES
			if(save == 0){ //dld h selida den exei apothikeytei
				ipt[ipt_index].pid = addr.pid;
				strcpy(ipt[ipt_index].page_number,addr.page_number);
				strcpy(main_memory[ipt_index].status,addr.status);
				
				if(main_memory[ipt_index].status[0] == 'R'){
					read_counter++;
				}
				ipt_index++;

			}
		}
	}

}//telos megalou for gia oles tis epanalipseis


printf("synolikes eggrafes ston disko (write back): %d\n",write_back_counter);
printf("synolikes anagnwseis apo disko (read): %d\n",read_counter);
printf("synolika sfalmata selidas (page faults): %d\n",overall_page_faults);
printf("oi kataxwrhseis pou eksetasthkan apo to arxeio ixnous: %d\n",max_traces);
printf("megisto plithos frames pou xrisimopoihthikan: %d\n",ipt_index);

fclose(fp1);
fclose(fp2);

return 0;
}
